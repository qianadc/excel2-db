-- Create table
create table ${table.name}
(
<#list table.attributeList as attribute>
${attribute.name} ${attribute.dataType}<#if !attribute.emptyFlag> not null</#if><#if attribute_has_next>,</#if>
</#list>
);
-- Add comments to the columns
<#list table.attributeList as attribute>
comment on column ${table.name}.${attribute.name} is '${attribute.remarks!''}';
</#list>

<#if table.primaryList?? && (table.primaryList?size>0)>
-- Create/Recreate primary, unique and foreign key constraints
alter table ${table.name}
    add constraint PK_${table.name} primary key (<#list table.primaryList as attribute>${attribute}<#if attribute_has_next>,</#if></#list>);
</#if>
