package org.qh.excel2db.config;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.qh.excel2db.entries.Attribute;
import org.qh.excel2db.entries.Table;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author qh
 * @create 2023/1/31 10:24
 */
@Slf4j
public class DataListener implements ReadListener<Attribute> {
    private List<Attribute> attributeList = new ArrayList<>();
    private List<String> primaryList = new ArrayList<>();
    public void invoke(Attribute data, AnalysisContext context) {
        if (data.getEmptyFlag() == null){
            data.setEmptyFlag(false);
        }
        if (data.getPrimaryFlag() == null){
            data.setPrimaryFlag(false);
        }
        if (data.getPrimaryFlag()){
            primaryList.add(data.getName());
        }
        attributeList.add(data);
    }

    /**
     * 针对多个sheet会在每个sheet读取完后自动调用
     *
     * @param context
     */
    public void doAfterAllAnalysed(AnalysisContext context) {
        String sheetName = context.readSheetHolder().getReadSheet().getSheetName();
        Table table = tableBuild(sheetName);
        createSql(table);
        attributeList = new ArrayList<>();
        primaryList = new ArrayList<>();
    }

    /**
     * 构建Table对象
     * @param sheetName
     * @return
     */
    private Table tableBuild(String sheetName) {
        Table table = new Table();
        int index = sheetName.indexOf('(');
        String name = sheetName;
        if (index > 0){
            name = sheetName.substring(0, sheetName.indexOf('('));
        }
        table.setName(name);
        table.setAttributeList(attributeList);
        table.setPrimaryList(primaryList);
        clearList();
        return table;
    }

    private void clearList() {
        attributeList = new ArrayList<>();
        primaryList = new ArrayList<>();
    }

    private void createSql(Table table) {
        // TODO 模板加载功能优化
        log.info("---{}",table);
        String dir = "F:\\Java\\excel2-db\\src\\main\\resources";
        Configuration conf = new Configuration(Configuration.VERSION_2_3_0);
        try {
            //加载模板文件(模板的路径)
            conf.setDirectoryForTemplateLoading(new File(dir));
            // 加载模板
            Template template = conf.getTemplate("/oracleTable.ftl");
            Map root = new HashMap();
            root.put("table", table);

            // TODO 模板输出功能优化
            Writer out = new FileWriter(dir + "/"+table.getName()+".sql");
            template.process(root, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
