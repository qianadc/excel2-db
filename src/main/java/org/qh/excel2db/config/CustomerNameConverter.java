package org.qh.excel2db.config;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;

/**
 * @author qh
 * @create 2023/1/31 14:14
 */
public class CustomerNameConverter implements Converter<String> {
    @Override
    public String convertToJavaData(ReadConverterContext<?> context) {
        String stringValue = context.getReadCellData().getStringValue();
        if (stringValue == null){
            return stringValue;
        }
        return stringValue.toUpperCase();
    }
}
