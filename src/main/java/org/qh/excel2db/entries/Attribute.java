package org.qh.excel2db.entries;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.qh.excel2db.config.CustomerBooleanConverter;
import org.qh.excel2db.config.CustomerNameConverter;
import org.qh.excel2db.enums.IndexEnum;

import java.util.List;

/**
 * @author qh
 * @create 2023/1/31 11:08
 */
@Getter
@Setter
@ToString
public class Attribute {
    @ExcelProperty(converter = CustomerNameConverter.class)
    private String name;

    private String dataType;

    @ExcelProperty(converter = CustomerBooleanConverter.class)
    private Boolean emptyFlag;

    @ExcelProperty(converter = CustomerBooleanConverter.class)
    private Boolean primaryFlag;

    private String remarks;

    private List<IndexEnum> indexList;

    private Integer indexNum;
}
