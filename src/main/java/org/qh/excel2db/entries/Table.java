package org.qh.excel2db.entries;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.qh.excel2db.enums.IndexEnum;

import java.util.List;

/**
 * @author qh
 * @create 2023/1/31 10:13
 */
@Getter
@Setter
@ToString
public class Table {
    private String name;
    private List<Attribute> attributeList;
    private List<String> primaryList;
}
