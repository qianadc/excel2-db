package org.qh.excel2db;

import com.alibaba.excel.EasyExcel;
import org.qh.excel2db.config.DataListener;
import org.qh.excel2db.entries.Attribute;

/**
 * @author qh
 * @create 2023/1/31 10:13
 */
public class RunningApplication {
    public static void main(String[] args) {
        EasyExcel.read("E:\\1.xlsx", Attribute.class, new DataListener()).doReadAll();
    }
}
